# programming resources

A collection of interesting articles and thoughtworthy links.


## Blogs

- [It's FOSS](https://itsfoss.com)
- [Julia Evans](https://jvns.ca)
- [LINux on MOBile](https://linmob.net)
- [Liliputing](https://liliputing.com)

## Articles

- [Tutorial: Remote debug](https://www.jetbrains.com/help/idea/tutorial-remote-debug.html)
- [Debug your Java applications in Docker using IntelliJ IDEA](https://blog.jetbrains.com/idea/2019/04/debug-your-java-applications-in-docker-using-intellij-idea/)
- [HikariCP: About Pool Sizing](https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing)
- [Understanding how OSGI bundles get resolved part I](https://blog.christianposta.com/osgi/understanding-how-osgi-bundles-get-resolved-part-i/)
- [How to make good code reviews better](https://stackoverflow.blog/2019/09/30/how-to-make-good-code-reviews-better/)
- [Yarn and the dark future of third party NPM clients](https://www.thedevcoach.co.uk/yarn-npm-future/)
- [C++ FAQ: Which is more efficient:i++ or ++i?](http://www.cs.technion.ac.il/users/yechiel/c++-faq/increment-pre-post-speed.html)
- [Using Kafka with Spring Boot](https://reflectoring.io/spring-boot-kafka/)
- [Protect our Git Repos, Stop Foxtrots Now!](https://blog.developer.atlassian.com/stop-foxtrots-now/)
- [LinkedIn’s Alternate Universe](https://divinations.substack.com/p/linkedins-alternate-universe)
- [Kubernetes pods /etc/resolv.conf ndots:5 option](https://pracucci.com/kubernetes-dns-resolution-ndots-options-and-why-it-may-affect-application-performances.html)
- [Publishing apps to the OpenStore with GitLab CI](https://blog.bhdouglass.com/clickable/tutorial/2019/03/18/publishing-apps-to-the-0penstore-with-gitlab-ci.html)
- [ANX7688 USB-C HDMI bridge on PinePhone (+ USB-C PD)](https://xnux.eu/devices/feature/anx7688.html)
- [An Overview of JavaScript Testing in 2020](https://medium.com/welldone-software/an-overview-of-javascript-testing-7ce7298b9870)
- [Master the JavaScript Interview: What is a Promise?](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261)
- [A Post-Mortem in 5 Acts: How Microsoft Privatized Open Source And Killed JavaScript in the Process](https://hackernoon.com/a-post-mortem-in-5-acts-how-microsoft-privatized-open-source-and-killed-javascript-in-the-process-5s5i33ma)
- [Loading npm dependencies from multiple registries](https://www.eckher.com/c/21g2_hpfhs)
- [Better BASHing Through Technology](https://andydote.co.uk/2020/08/28/better-bashing-through-technology/)
- [Documenting Architecture Decisions](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)
- [I forgot my sudo password](https://superuser.com/questions/1348315/i-forgot-my-sudo-password)

## Discussions

- [JSON:API - Representing non-resourceful aggregated data](https://discuss.jsonapi.org/t/representing-non-resourceful-aggregated-data/431)
- [semver: Spec doesn't indicate whether pre-release labels and build metadata are case sensitive](https://github.com/semver/semver/issues/176)
- [Hikari connection pool grows to maximum size at start](https://github.com/brettwooldridge/HikariCP/issues/256#issuecomment-73641491)
- [docker-compose up vs docker-compose up --build vs docker-compose build --no-cache](https://stackoverflow.com/questions/39988844/docker-compose-up-vs-docker-compose-up-build-vs-docker-compose-build-no-cach)
- [How do the post increment (i++) and pre increment (++i) operators work in Java?](https://stackoverflow.com/questions/2371118/how-do-the-post-increment-i-and-pre-increment-i-operators-work-in-java/)
- [How do I get templated values in uri tag for http-client-requests metrics using Spring-Boot's RestTemplate?](https://stackoverflow.com/questions/58942924/how-do-i-get-templated-values-in-uri-tag-for-http-client-requests-metrics-using)
- [Dealing with conflicts when merging from development to master](https://stackoverflow.com/questions/19516060/dealing-with-conflicts-when-merging-from-development-to-master)

## Tooling

- [shellcheck](https://github.com/koalaman/shellcheck)
- [alpine-shellcheck](https://github.com/NLKNguyen/alpine-shellcheck)
- [alpine-node](https://github.com/mhart/alpine-node)
- [helm-docs](https://github.com/norwoodj/helm-docs#installation)
- [skaffold](https://github.com/GoogleContainerTools/skaffold)
- [openapi-generator](https://github.com/OpenAPITools/openapi-generator/)
- [codespell](https://github.com/codespell-project/codespell)
- [jcmd](https://docs.oracle.com/en/java/javase/11/tools/jcmd.html)
- [jenv](https://github.com/jenv/jenv)
- [groovysh](http://groovy-lang.org/groovysh.html)
- [Robots.txt Testing Tool](https://logeix.com/tools/robots-txt)

## References

- [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml)
- [Spock Framework Reference Documentation](http://spockframework.org/spock/docs/1.3/all_in_one.html)
- [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
- [Helm Best Practices](https://helm.sh/docs/chart_best_practices)
- [Dockerfile Best Practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [Philipp Koch: Docker best practices (presentation)](https://www.slideshare.net/PhilippKoch11/docker-best-practices)
- [Bootstrap overview](https://getbootstrap.com/docs/4.4/layout/overview)
- [Spring Boot Common Application properties](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html)
- [Baeldung](https://www.baeldung.com)
- [ThoughtWorks: Technology Radar](https://www.thoughtworks.com/radar)
- [PostgreSQL: Using Java 8 Date and Time classes](https://jdbc.postgresql.org/documentation/head/8-date-time.html)
- [The Java Version Almanac](https://javaalmanac.io/)
- [UTF-8 Everywhere](https://utf8everywhere.org/)
- [Snapcraft.yaml reference](https://snapcraft.io/docs/snapcraft-yaml-reference)
- [Flatpak Manifest reference](https://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html#flatpak-manifest)
- [Node.js Stream reading modes](https://nodejs.org/api/stream.html#stream_two_reading_modes)
- [Emmet Cheat sheet](https://docs.emmet.io/cheat-sheet/)
- [2020 JavaScript Rising Stars](https://risingstars.js.org/2020/en)
- [Translation of Excel functions](https://www.excelfunctions.eu/)
- [LibHunt](https://www.libhunt.com/)
- [GitLab: Upgrade paths](https://docs.gitlab.com/ee/update/#upgrade-paths)

## Specifications and standards

- [arc42 documentation template](https://arc42.org/overview)
- [JSON API Specification](https://jsonapi.org/format)
- [Semantic versioning](https://semver.org)
- [Editorconfig](https://editorconfig.org)
- [Open API specification](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [All Contributors](https://allcontributors.org/)

## Books

- [Learn you a Haskell](http://learnyouahaskell.com/)

## Talks

- [NDC: The Art of Code - Dylan Beattie](https://www.youtube.com/watch?v=6avJHaC3C2U)
- [10 Things I Regret About Node.js - Ryan Dahl - JSConf EU](https://www.youtube.com/watch?v=M3BM9TB-8yA)
- [CHEAP WI-FI MESH ALTERNATIVE with fast roaming OpenWrt Wi-Fi Access points](https://www.youtube.com/watch?v=kMgs2XFClaM)

## Tutorials

- [Learn Git Branching](https://learngitbranching.js.org/)
- [irssi: New users guide](https://irssi.org/New-users/)


Repository icon made by [Good Ware](https://www.flaticon.com/authors/good-ware) found at [Flaticon](https://www.flaticon.com).
